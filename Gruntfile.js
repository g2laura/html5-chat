module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    watch: {
      options: {
        livereload: true
      },
      js: {
        files: ['www/js/*.js'],
        tasks: ['jshint:beforeconcat','concat:dist','jshint:afterconcat'],
      },
      less: {
        files: ['www/css/*.less'],
        tasks: ['less:dev','autoprefixer','csslint'],
      },
      html: {
        files: ['www/*.html'],
      }
    },
    concat: {
      dist: {
        src: ['www/js/date.format.js','www/js/global.js','www/js/connection.js','www/js/user.js','www/js/contacts.js','www/js/messages.js','www/js/emoticons.js','www/js/chat.js','www/js/history.js', 'www/js/circular-menu.js', 'www/js/profile-presence.js'],
        dest: 'www/js/chat-concat.js'
      },
      less: {
        src: ['www/css/style.less'],
        dest: 'www/css/style.less'
      }
    },
    jshint: {
      beforeconcat: ['www/js/global.js','www/js/connection.js','www/js/user.js','www/js/contacts.js','www/js/messages.js','www/js/emoticons.js','www/js/chat.js','www/js/history.js','www/js/circular-menu.js', 'www/js/profile-presence.js'],
      afterconcat: 'www/js/chat-concat.js'
    },
    less: {
      options: {
        paths: ['www/css'],
        'strictMath': true
      },
      dev: {
        files: {
          'www/css/style.css': 'www/css/style.less'
        }
      },
      emoji: {
        files: {
          'www/css/emoji.css': 'www/css/emojify.less'
        }
      },
      prod: {
        options: {
          yuicompress: true,
          optimizacion: 3
        },
        files: {   
          'www/css/style.css': 'www/css/style-concat.less'
        }
      }
    },
    csslint: {
      strict: {
        options: {
          'qualified-headings': false,
          'overqualified-elements': false,
          'box-model': false,
          'adjoining-classes': false,
          'unique-headings': false,
          'duplicate-background-images': false,
          'font-sizes': false,
          'box-sizing': false,
          'compatible-vendor-prefixes': false,
          'floats': false,
          import: false
        },
        src: ['www/css/style.css']
      }
    },
    uglify: {
      options: {
        banner: 'Chat <%= grunt.template.today("yyyy-mm-dd") %> \n'
      },
      build: {
        src: 'src/chat.js',
        dest: 'build/chat.min.js'
      }
    },
    clean: {
      build: ['www/js/chat-concat.js','www/css/style.css']
    },
    autoprefixer: {
      options: {
        browsers: ['last 3 versions', 'ff 3.6', 'android 2.2']
      },
      dist: {
        files: {
          'www/css/style.css': ['www/css/style.css']
        }
      }
    }
  });

  // Load all the plugins
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-csslint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');
  grunt.loadNpmTasks('grunt-notify');
  grunt.loadNpmTasks('grunt-autoprefixer');

  // Default task(s).
  grunt.registerTask('default', ['jshint:beforeconcat','concat','jshint:afterconcat','concat:less','less:dev','autoprefixer','csslint','watch']);
  grunt.registerTask('release', ['jshint','less','csslint']);
  grunt.registerTask('emoji', ['less:emoji','csslint']);

};
