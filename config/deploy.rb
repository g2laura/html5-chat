set :application, "chat"
set :repository,  "git@git.divux.com:chat/html5.git"

set :deploy_to, "/srv/apps/com/divux/chat"
set :deploy_via, :rsync_with_remote_cache

set :user, 'root'

server 'chatea.divux.com', :app, :web, :db, :primary => true

after "deploy:symlink", "deploy:fix_perms"

namespace :deploy do
  task :fix_perms do
    run "cd #{current_path} && #{try_sudo} chown -R www-data:www-data ."
    run "cd #{current_path} && #{try_sudo} chmod -R 755 www/"
  end
end

namespace :grunt do
  task :run do
    run "cd #{current_path} && npm install"
  end
end
