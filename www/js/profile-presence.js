function showPresenceOptions(p) {
  var options = ["available", "away", "dnd", "unavailable"];
  var optionsNames = ["Disponible", "Ausente", "Ocupado", "Desconectado"];
  var opacity = '';

  for (i=0; i<options.length; i++) {
    if (user.presence != options[i]) {
      opacity = "transparent";
    } else {
      opacity = '';
    }
    html = '<p><span class="presence presence-'+options[i]+' '+opacity+'"></span><span class="presence'+' '+opacity+'">'+optionsNames[i]+'</span></p>';
    p.parent().append(html);
  }
}

function refreshOptions(option) {
  $(".presence").parent().each(function(key,item) {
    if ($(item).text() != option) {
      $(item).children("span").addClass("transparent");
    }
  });
}

function getSelectedPresenceOption() {
  var selected = '';
  $(".presence").parent().each(function(key,item) {
    if ($(item).children("span").first().hasClass("transparent") === false) {
      selected = $(item).children("span").first().attr('class').split("-")[1]; 
    }
  });
  return $.trim(selected);
}

function changePresence() {
  var newPresence = getSelectedPresenceOption();
  $(".presence").parent().remove();
  $('#profile-info article').append("<p></p>");
  user.presence = newPresence;
  var html = user.userPresence(newPresence);
  $("#profile-info article p").first().next().html(html);
}