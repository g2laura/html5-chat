// Global variables
var DB_NAME = 'Chat';
var DB_VERSION = 8;
var DB_STORE_NAME = 'messages';
var db;
var lastMessages = [];
var lastContacts = [];
var sliceMessages = [];
var lastJidMessage = '';
var lastGroupMessage = '';
var userGroups = [];
var groupMessagesNumber;
var groupParticipants = [];

// Create/Open DB
function openDb() {

  var req = indexedDB.open(DB_NAME, DB_VERSION);
  
  req.onsuccess = function (evt) {
    db = this.result;
    checkSession();
  };

  req.onerror = function (evt) {
    var errorMessage = evt.target.errorCode;

    // indexedDB.polyfill specific errors
    if (errorMessage === undefined && evt.target.error && evt.target.error.inner) {
      errorMessage = evt.target.error.inner.name;
    } else {
      errorMessage = 'Unknown';
    }

    console.error('Database error: ' + errorMessage);
  };
  
  req.onupgradeneeded = function (evt) {
    var thisDB = evt.target.result,
        store;

    // messages
    if(!thisDB.objectStoreNames.contains(DB_STORE_NAME)) {
      store = evt.currentTarget.result.createObjectStore(
        DB_STORE_NAME, { keyPath: 'timestamp'});
      store.createIndex('timestamp', ['user','jid'], { unique: false });
      store.createIndex('type', 'type', { unique: false });
      store.createIndex('user', 'user', { unique: false });
    }

    //Fix change of domain
    if (evt.oldVersion < 3) {
      var objectStore = evt.target.transaction.objectStore(DB_STORE_NAME);

      objectStore.openCursor().onsuccess = function(event) {
        var cursor = event.target.result;
        if (cursor) {
          var message = cursor.value;
          message.jid = message.jid.replace('chat.divux.com', 'chatea.divux.com');
          message.user = message.user.replace('chat.divux.com', 'chatea.divux.com');
          objectStore.put(message);
          cursor['continue']();
        }
        return;
      };
    }

    if (evt.oldVersion < 4) {
      if(thisDB.objectStoreNames.contains("sessions")) {
        evt.currentTarget.result.deleteObjectStore("sessions");
      }
      var sessions = evt.currentTarget.result.createObjectStore(
          "sessions", { keyPath: 'userSession'});
      sessions.createIndex('userSession', 'userSession', { unique: false });
      store = evt.target.transaction.objectStore(DB_STORE_NAME);
      store.createIndex('id', 'id', { unique: false });
      store.createIndex('status', ['jid', 'status'], { unique: false });
      //Set status to every message till now
      store.openCursor().onsuccess = function(evt) {
        var cursor = evt.target.result;
        if (cursor) {
          var message = cursor.value;
          message.status = 'seen';
          store.put(message);
          cursor['continue']();
        }
        return;
      };
    }

    // Added settings for each user notifications with or without sound.
    if (evt.oldVersion < 6) {
      var config = evt.currentTarget.result.createObjectStore("settings", { keyPath: 'settingId', autoIncrement:true });
      config.createIndex('setting', ['jid', 'setting'], { unique: false });
    }    

    // Groups
    if (evt.oldVersion < 7) {
      var groups = evt.currentTarget.result.createObjectStore("groups", { keyPath: 'jid'});
      groups.createIndex('jid', 'member', { unique: false });
    }

    // Groups invites
    if (evt.oldVersion < 8) {
      var groupsInvites = evt.currentTarget.result.createObjectStore("groups_invited", { keyPath: 'timestamp'});
      groupsInvites.createIndex('groupInvitationTimestamp', ['group','invited'], { unique: false });
      groupsInvites.createIndex('invited', 'invited', { unique: false });
    }
  };
}

// Store message
function storeMessage(message){
  var transaction = db.transaction([DB_STORE_NAME], "readwrite");
  var objectStore = transaction.objectStore(DB_STORE_NAME);
  var request = objectStore.add(message);
}

// Store groups
function storeGroup(name){
  var transaction = db.transaction("groups", "readwrite");
  var objectStore = transaction.objectStore("groups");
  var request = objectStore.put(name);
}

// Store groups_invited
function storeGroupInvites(invite){
  var transaction = db.transaction("groups_invited", "readwrite");
  var objectStore = transaction.objectStore("groups_invited");
  var request = objectStore.add(invite);
}

// Store Session
function storeSession(session){
  var transaction = db.transaction(["sessions"], "readwrite");
  var objectStore = transaction.objectStore("sessions");
  var request = objectStore.put(session);
}

// Store User Settings
function storeSettings(setting){
  var transaction = db.transaction("settings", "readwrite");
  var objectStore = transaction.objectStore("settings");
  var index = objectStore.index("setting");
  var range = IDBKeyRange.only([user.jid, "notification"]);

  index.openCursor(range).onsuccess = function(event) {
    var cursor = event.target.result;
    if (cursor) {
      oldSetting = cursor.value;
      oldSetting.value = setting.value;
      objectStore.put(oldSetting);
    } else {
      objectStore.put(setting);
    }
    return;
  };

  if(setting.setting === "notification"){
    notificationsWithSound = setting.value;
    if (notificationsWithSound=='true') {
      $("#config-notifications").attr('checked', 'checked');
    } else {
      $("#config-notifications").removeAttr('checked');
    }
  }
}

// Get last stored messages betwen user and a jid conact
function getLastStoredMessages(user, jid , start, end) {
  var transaction = db.transaction([DB_STORE_NAME], "readwrite");
  var objectStore = transaction.objectStore(DB_STORE_NAME);
  var index = objectStore.index("timestamp");
  var range = IDBKeyRange.only([user,jid]);
  var talk = [];

  index.openCursor(range).onsuccess = function(event) {
    var cursor = event.target.result;
    if (cursor) {
      talk.push(cursor.value);
      cursor['continue']();
    } else {
      lastMessages = lastMessages.concat(talk);
      if ( lastMessages.length > 0) {
        $('#history').show();
        $('#messages').removeClass("adjust");
        lastMessages.reverse();
        sliceMessages = lastMessages.slice(start,end);
        $.each(sliceMessages, function(index, item){
          if (item.type === "sended") {
            position = "right";
          } else {
            position = "left";
          }
          chat.showMessage(item.jid, item.message, item.timestamp,position, "receive","top", item.id, item.status, item.chatType, item.nick);
        });
        validateContactWriting(jid);
      } else {
        $('#history').hide();
        $('#messages').addClass("adjust");
      }
      return;
    }
  };
}

// Validates if contact is writing a message
function validateContactWriting(jid) {
  var isContactWriting = $("div[data-jid='"+ jid +"']").find(".contact-composing").hasClass("composing");
  var lastContactMessage = $('.bubble.left');
  if (isContactWriting) {
    lastContactMessage.last().find(".writing").addClass("show-writing");
  } else {
    lastContactMessage.eq(-2).find(".writing").removeClass("show-writing");
  }
}

// Show last Talk between user and a jid contact 
function showLastTalk(jid, start, end){
  var position = "";
  lastMessages.length = 0;
  getLastStoredMessages(user.jid, jid , start, end);
}

// Get last stored message from contact
function getLastMessage(user, jid) {
  var transaction = db.transaction([DB_STORE_NAME], "readwrite");
  var objectStore = transaction.objectStore(DB_STORE_NAME);
  var index = objectStore.index("timestamp");
  var range = IDBKeyRange.only([user,jid]);
  var talk = [];

  index.openCursor(range).onsuccess = function(event) {
    var cursor = event.target.result;
    if (cursor) {
      lastJidMessage = cursor.value;
      cursor['continue']();
    } else {
      var contactRow = $("#talk-list div[data-jid='"+ jid +"']");
      var message = contactRow.find('.last-message');
      message.text(lastJidMessage.message);
      emojify.run(message[0]);
      if (contactRow.find(".contact-composing").hasClass("composing")) {
        message.hide();
      } else {
        message.show();
      }
      return;
    }
  };
}

// Get last contacts with messages
function getLastContactsTalk(user) {
  var transaction = db.transaction([DB_STORE_NAME], "readwrite");
  var objectStore = transaction.objectStore(DB_STORE_NAME);
  var index = objectStore.index("user");
  var range = IDBKeyRange.only(user);
  var contactList = [];

  index.openCursor(range).onsuccess = function(event) {
    var cursor = event.target.result;
    var contact = "";
    if (cursor) {
      if ($.inArray(cursor.value.jid,contactList) === -1) {
        contactList.push(cursor.value.jid);
      }
      cursor['continue']();
    } else {
      lastContacts = lastContacts.concat(contactList);
      lastContacts.splice(contactLimit);
      $.each(lastContacts, function(index, item){
        contact = contacts.getContact(item);
        contacts.addContactToList(contact, 'talk');
        if (contact.messages !== undefined) {
          contacts._refreshContactVariable(contact.jid, "messages", contact.messages);
        }
        getLastMessage(user, contact.jid);
      });
      getUserGroups();
      return;
    }
  };
}

// Groups
function getUserGroups() {
  var transaction = db.transaction("groups", "readwrite");
  var objectStore = transaction.objectStore("groups");
  var index = objectStore.index("jid");
  var range = IDBKeyRange.only(user.jid);
  var contact = '';

  index.openCursor(range).onsuccess = function(event) {
    var cursor = event.target.result;
    if (cursor) {
      if (contacts.getContact(cursor.value.jid) === null) {
        contacts.list.push(cursor.value);
      }
      cursor['continue']();
    } else {
      $.each(contacts.list, function(index, item){
        if (contacts.isGroup(item.jid)) {
          contact = contacts.getContact(item.jid);
          contacts.addContactToList(contact, 'talk');
          countGroupMessages(user.jid, item.jid);
        }
      });
      $('.talk-list .contact-favorite').hide();
      $('.talk-list .contact-delete').show();
    }
  };
}

// Get Session by user
function checkSession() {
  var transaction = db.transaction(["sessions"], "readwrite");
  var objectStore = transaction.objectStore("sessions");
  var index = objectStore.index("userSession");
  index.openCursor().onsuccess = function(event) {
    var cursor = event.target.result;
    if (cursor) {
      action = "login";
      user.jid = cursor.value.userSession;
      Connection.connect(user.jid + '/'+user.resource, cursor.value.password, connectionURL);
      user.jid = cursor.value.userSession;
      cursor['continue']();
    }
  };
}

// Get Settings by user
function checkSettings() {
  var transaction =  db.transaction(["settings"], "readwrite");
  var objectStore = transaction.objectStore("settings");
  var index = objectStore.index("setting");
  var range = IDBKeyRange.only([user.jid, "notification"]);

  index.openCursor(range).onsuccess = function(event) {
    var cursor = event.target.result;
    if (cursor) {
      notificationsWithSound = cursor.value.value;
      if (notificationsWithSound=='true') {
        $("#config-notifications").attr('checked', 'checked');
      } else {
        $("#config-notifications").removeAttr('checked');
      }
      cursor['continue']();
    }
  };
}

function deleteSession(user) {
  var transaction = db.transaction(["sessions"], "readwrite");
  var objectStore = transaction.objectStore("sessions");
  var request = objectStore['delete'](user);
}

function deleteGroup(group) {
  var transaction = db.transaction(["groups"], "readwrite");
  var objectStore = transaction.objectStore("groups");
  var request = objectStore['delete'](group);
}

function existsGroup(group) {
  var transaction = db.transaction(["groups"], "readwrite");
  var objectStore = transaction.objectStore("groups");
  var request = objectStore.get(group);
  request.onerror = function(event) {
    return false;
  };
  request.onsuccess = function(event) {
    return true;
  };
}

function updateMessageStatus(id, status) {
  var transaction = db.transaction([DB_STORE_NAME], "readwrite");
  var objectStore = transaction.objectStore(DB_STORE_NAME);
  var index = objectStore.index("id");

  index.get(id).onsuccess = function(event) {
    var message = event.target.result;
    if (message !== undefined) {
      message.status = status;
      objectStore.put(message);
    }
    return;
  };
}

function updateMessageStatusBulk(jid, status) {
  var transaction = db.transaction([DB_STORE_NAME], "readwrite");
  var objectStore = transaction.objectStore(DB_STORE_NAME);
  var index = objectStore.index('status');
  var range = IDBKeyRange.only([jid, 'received']);
  var contactList = [];

  index.openCursor(range).onsuccess = function(event) {
    var cursor = event.target.result;
    if (cursor) {
      var message = cursor.value;
      message.status = status;
      objectStore.put(message);
      cursor['continue']();
    }
    return;
  };
}

function deleteGroupMessages(user, jid) {
  var transaction = db.transaction([DB_STORE_NAME], "readwrite");
  var objectStore = transaction.objectStore(DB_STORE_NAME);
  var index = objectStore.index("timestamp");
  var range = IDBKeyRange.only([user,jid]);

  index.openCursor(range).onsuccess = function(event) {
    var cursor = event.target.result;
    if (cursor) {
      cursor.delete();
      cursor['continue']();
    } else {
      return;
    }
  };
}

function deleteGroupInvites(group, invited) {
  var transaction = db.transaction("groups_invited", "readwrite");
  var objectStore = transaction.objectStore("groups_invited");
  var index = objectStore.index("groupInvitationTimestamp");
  var range = IDBKeyRange.only([group,invited]);

  index.openCursor(range).onsuccess = function(event) {
    var cursor = event.target.result;
    if (cursor) {
      cursor.delete();
      cursor['continue']();
    } else {
      return;
    }
  };
}

function countGroupMessages(user, jid) {
  var transaction = db.transaction([DB_STORE_NAME], "readwrite");
  var objectStore = transaction.objectStore(DB_STORE_NAME);
  var index = objectStore.index("timestamp");
  var range = IDBKeyRange.only([user,jid]);
  groupMessagesNumber = 0;

  index.openCursor(range).onsuccess = function(event) {
    var cursor = event.target.result;
    if (cursor) {
      groupMessagesNumber++;
      cursor['continue']();
    } else {
      if (groupMessagesNumber === 0) {
        var lastMessage = $("#talk-list div[data-jid='"+ jid +"']").find('.new-group');
        lastMessage.text("Grupo Nuevo");
      }
    }
  };
}

function getUserInvites(user) {
  var transaction = db.transaction("groups_invited", "readwrite");
  var objectStore = transaction.objectStore("groups_invited");
  var index = objectStore.index("invited");
  var range = IDBKeyRange.only(user);

  index.openCursor(range).onsuccess = function(event) {
    var cursor = event.target.result;
    if (cursor) {
      groupInvite (user.jid, cursor.value.group, cursor.value.invited);
      deleteGroupInvites(cursor.value.group, cursor.value.invited);
      cursor['continue']();
    } else {
      return;
    }
  };
}

function sendUserGroupsPresence() {
  var transaction = db.transaction("groups", "readwrite");
  var objectStore = transaction.objectStore("groups");
  var index = objectStore.index("jid");
  var range = IDBKeyRange.only(user.jid);
  var contact = '';

  index.openCursor(range).onsuccess = function(event) {
    var cursor = event.target.result;
    if (cursor) {
      chat.sendPresenceToGroup(cursor.value.jid);
      cursor['continue']();
    }
  };
}

function showDelayMessage(message, jid, response, time, chatType, id) {
  var transaction = db.transaction([DB_STORE_NAME], "readwrite");
  var objectStore = transaction.objectStore(DB_STORE_NAME);
  var index = objectStore.index("id");
  var range = IDBKeyRange.only(id);

  index.openCursor(range).onsuccess = function(event) {
    var cursor = event.target.result;
    if (!cursor) {
      chat.processReceivedMessage(message, jid, response, time, chatType, id);
    }
  };
}

$(document).ready(function() {
  $(".last-chats").on(clickEvent, function() {
    lastContacts.length = 0;
    getLastContactsTalk(user.jid);
  });
});