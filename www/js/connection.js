var connectionURL = 'ws://' + serverAddress + '/ws';
var action = null;
var silent = false;

//Detect WebSocket support
if (!window.WebSocket) {
  window.WebSocket=window.MozWebSocket;
  if (!window.WebSocket) {
    connectionURL = 'http://' + serverAddress + '/http-bind';
  }
}

$(document).ready(function() {
  $('#login-form').on('submit', function(e){
    closedSession = false;
    e.preventDefault();
    user.jid = $('#user').val() + '@' + serverAddress;
    Connection.connect(user.jid + '/' + user.resource, $('#password').val(), connectionURL);
    storeSession({userSession: user.jid, password: $('#password').val()});
    action = 'login';
    return false;
  });

  $('#signup-form').on('submit', function(e){
    e.preventDefault();
    Connection.connect('signup@chatea.divux.com' + '/' + user.resource, '12345', connectionURL);
    action = 'signup';
    return false;
  });

  $('#registerAccount').on(clickEvent, function(e){
    e.preventDefault();
    $('body > section').hide();
    $('#signup').show();
  });
});

function disconnect() {
  Connection.sendRequest($pres().c('show').t("unavailable"));
  Connection.disconnect();
}

var Connection = {
  connection : null,

  connect : function (user, password, address) {
    this.connection = new Strophe.Connection(address);
    this.connection.rawOutput = this.rawOutput;
    this.connection.rawInput = this.rawInput;
    this.connection.connect(user, password, this._onConnection);
  },

  _onConnection : function(status) {
    if (status == Strophe.Status.CONNECTING) {
      //TODO
    } else if (status == Strophe.Status.CONNFAIL) {
      //TODO
    } else if (status == Strophe.Status.AUTHFAIL) {
      $('.circular-nav').hide();
      $('body > section').hide();
      $('#login').show();
      $('#login .alert-error').html('Ingreso un usuario o contraseña incorrecta. Intente de nuevo.').show();
      deleteSession(user.jid);
    } else if (status == Strophe.Status.AUTHENTICATING) {
      //TODO
    } else if (status == Strophe.Status.DISCONNECTING) {
      //TODO
    } else if (status == Strophe.Status.DISCONNECTED) {
      if (closedSession === true) {
        $('.circular-nav').hide();
        $('body > section').hide();
        $('#login').show();
      } else {
        silent = true;
        checkSession();
      }
    } else if (status == Strophe.Status.CONNECTED) {
      if (!silent) {
        $('body > section').hide();
      }
      $('.alert-error').html('').hide();
      $('.alert-success').html('').hide();
      var request;
      if (action == 'login') {
        request = $iq({to:serverAddress, type:'set'}).c('session', {xmlns:Strophe.NS.SESSION});
        Connection.sendRequest(request);
        request = $pres();
        Connection.sendRequest(request);
        if (!silent) {
          $('.circular-nav').show();
          $('#last-chats, #settings').show();
          request = $iq({type:'get'}).c('query', {xmlns:Strophe.NS.ROSTER});
          Connection.sendRequest(request);
          user.getStorage();
          user.getVCard(user.jid);
        }
        user.presence = "available";

        Connection.connection.addHandler(function(stanza) {
          determineMessage(stanza);
          return true;
        }, null, 'iq');

        Connection.connection.addHandler(function(stanza) {
          messages.listPresence(stanza);
          return true;
        }, null, 'presence');

        Connection.connection.addHandler(function(stanza) {
          chat.getMessage(stanza);
          return true;
        }, null, 'message');

        Connection.connection.addTimedHandler(20000, function() {
          var request = $iq({from:user.jid +'/'+user.resource, to:serverAddress, type:'set'}).c('ping',{xmlns:'urn:xmpp:ping'});
          Connection.sendRequest(request);
          return true;
        });

        Connection.connection.addTimedHandler(20000, function() {
          sendUserGroupsPresence();
          return true;
        });
        //Check user settings
        checkSettings();

      } else if (action == 'signup') {
        Connection.connection.addHandler(function(stanza) {
          determineMessage(stanza);
          return true;
        }, null, 'iq');

        $('#signup').show();
        request = $iq({type:'set', id: 'signup'}).c('query', {xmlns:'jabber:iq:register'});
        request.c('username').t($("#signupUsername").val());
        request.up().c('password').t($("#signupPassword").val());
        request.up().c('email').t($("#signupEmail").val());
        Connection.sendRequest(request);

      }
    }
  },

  disconnect : function() {
    $('.circular-nav').hide();
    $('#online-contacts-list').empty();
    $('#offline-contacts-list').empty();
    $('#favorite-list').empty();
    $('#talk-list').empty();
    contacts.list.length = 0;
    deleteSession(user.jid);
    user.reset();
    closedSession = true;
    this.connection.disconnect();
    silent = false;
  },

  sendRequest : function(request) {
    this.connection.send(request.tree());
  },

  getVCard : function(jid) {
    this.connection.vcard.get(messages.listVCard, jid);
  },

  rawInput : function(data) {
    console.log('RECV: ' + data);
  },

  rawOutput : function(data) {
    console.log('SENT: ' + data);
  }
};
