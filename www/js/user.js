var user = {
  jid      : null,
  resource : ("html5-" + guid()),
  photo    : null,
  status   : null,
  name     : null,
  nickname : null,
  presence : null,
  available : null,
  vcard    : null,
  hash     : null,

  setStorage : function(variable, value) {
    var request = $iq({type:'set', to:this.jid}).c('query',{xmlns:'jabber:iq:private'});
    request.c('client',{xmlns:'client:data'}).c(variable).t(value);
    Connection.sendRequest(request);
  },

  getStorage : function() {
    var request = $iq({type:'get'}).c('query',{xmlns:'jabber:iq:private'}).c('client',{xmlns:'client:data'});
    Connection.sendRequest(request);
  },

  getStorageVariable : function () {
    if (storageObject !== null) {
      var elements = storageObject.getElementsByTagName("client");
      if (elements.length > 0){
        if (elements[0].childNodes.length>0){
          var element = null;
          for(var i=0;i<elements[0].childNodes.length;i++){
            element = elements[0].childNodes[i];
            if (element.nodeName == "status"){
              user.status = getNodeValue(element);
              this.changeStatus(user.status);
            }
          }
        }
      }
    }
  },

  getPresence : function(userId) {
    var request = $pres({type:'probe', from:this.jid+'/'+this.resource, to:userId});
    Connection.sendRequest(request);
  },

  setPresence : function() {
    var request = $pres({from:this.jid+'/'+this.resource}).c('x',{xmlns:'vcard-temp:x:update'});
    request.c('photo').t(this.hash);
    Connection.sendRequest(request);
  },
  
  getVCard : function (jid) {
    Connection.getVCard(jid);
  },

  changeStatus : function(status) {
    var request = $pres().c('status').t(status);
    request.up().c('show').t(user.presence);
    Connection.sendRequest(request);
    this.setStorage("status", status);
  },

  setVCard : function() {
    var request = $iq({type:'set'}).c('vCard',{xmlns:'vcard-temp'});
    if (this.photo !== null) {
      request.c('PHOTO').c('TYPE').t(getPhotoValue(this.photo, 'type'));
      request.up().c('BINVAL').t(getPhotoValue(this.photo, 'base64'));
      if (this.nickname !== null) {
        request.up().up().c('NICKNAME').t(this.nickname);
      }
    }
    if (this.nickname !== null) {
      request.c('NICKNAME').t(this.nickname);
    }
    Connection.sendRequest(request);
    this.setPresence();
  },

  changePhoto : function() {
    var filesSelected = document.getElementById("profile-photo-file").files,
        fileReader = new FileReader();

    if (filesSelected.length > 0) {
      fileReader.onload = user.handleFile;

      fileReader.readAsDataURL(filesSelected[0]);
    }
  },

  refreshProfile : function() {
    var html = this.userPresence(this.presence);
    var p = $("#profile-info article p");
    p.first().html(this.status);
    p.first().next().html(html); 
    $("#profile-info article header h1").html(this.nickname !== null ? this.nickname : this.jid);   
    $("#profile-info figure img").attr("src" , this.photo !== null ? this.photo : "img/profile-photo.png");
  },

  changeNickname : function(nickname) {
    this.nickname = nickname;
    this.setVCard();
  },

  handleFile : function(fileLoadedEvent) {
    var newPhoto = (activity ? window.URL.createObjectURL(this.result.blob) : fileLoadedEvent.target.result);
    $("#profile-info figure img").attr("src" , newPhoto !== null ? newPhoto : "img/profile-photo.png");
    if (newPhoto !== null) {
      var img = new Image();
      img.onload = user._resizePhoto;
      img.src = newPhoto;
    }
  },

  _resizePhoto: function() {
    var canvas = document.getElementById("canvas"),
        context = canvas.getContext("2d"),
        MAX_WIDTH = 96,
        MAX_HEIGHT = 96,
        width = this.width,
        height = this.height;

    if (width > height) {
      if (width > MAX_WIDTH) {
        height *= MAX_WIDTH / width;
        width = MAX_WIDTH;
      }
    } else {
      if (height > MAX_HEIGHT) {
        width *= MAX_HEIGHT / height;
        height = MAX_HEIGHT;
      }
    }

    canvas.width = width;
    canvas.height = height;
    context.drawImage(this, 0, 0, width, height);

    user.photo = canvas.toDataURL('image/jpeg');
    user.hash = hex_sha1(user.photo);
    user.setVCard();
  },

  reset: function() {
    this.jid = null;
    this.resource = ("html5-" + guid());
    this.photo = null;
    this.status = null;
    this.name = null;
    this.nickname = null;
    this.presence = null;
    this.available = null;
    this.vcard = null;
    this.hash = null;
  },

  userPresence: function(type) {
    var presenceStatus = '', html= '';
    if (type === "available") {
      presenceStatus = "Disponible";
    } else if (type === "dnd" || type === "xa" ) {
      presenceStatus = "Ocupado";
    } else if (type === "away") {
      presenceStatus = "Ausente";
    } else if (type === "unavailable") {
      presenceStatus = "Desconectado";
    }
    html = '<span class="presence-'+type+'"></span>'+presenceStatus;
    return html;
  }
};
