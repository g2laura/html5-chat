var contacts = {
  LOG_TAG : "CONTACTS",

  list : [],

  contact : {
    nickname      : null,
    jid           : null,
    status        : null,
    presence      : null,
    groups        : [],
    messages      : 0,
    hash          : null,
    subscription  : null
  },

  getContact : function(jid) {
    for (var i = 0; i < this.list.length; i++) {
      if (this.list[i].jid === jid) {
        return this.list[i];
      }
    }
    return null;
  },

  isGroup : function(jid) {
    if (jid.split("@")[1] === conferenceAddress) {
      return true;
    }
    return false;
  },

  removeContact : function(jid) {
    for (var i = 0; i < this.list.length; i++) {
      if (this.list[i].jid === jid) {
        this.list.splice(i, 1);
        this.removeContactFromHtml(jid);
      }
    }
  },

  removeGroup : function(jid) {
    for (var i = 0; i < this.list.length; i++) {
      if (this.list[i].jid === jid) {
        this.list.splice(i, 1);
      }
    }
  },

  setContactVariable : function(jid, variable, value) {
    if (this.list.length > 0) {
      for (var i = 0; i < this.list.length; i++) {
        if (this.list[i].jid === jid) {
          this.list[i][variable] = value;
          this._refreshContactVariable(jid, variable, value);
        }
      }
    }
  },

  // Refresh an especific contact
  refreshContact : function(contact) {
    var oldContact = this.getContact(contact.jid),
        isFavorite = false,
        contactHtml = "",
        index = -1;
    if (oldContact !== null) {
      if (contact.subscription === 'subscribing') {
        this.removeContact(contact.jid);
        oldContact = null;
        contact.subscription = "subscribe";
      } else if (contact.subscription === 'remove') {
        this.removeContact(contact.jid);
      }
    }
    if (oldContact === null) {
      this.list.push(contact);
      if (contact.groups !== null) {
        index = contact.groups.indexOf('favorite');
      }

      if (index !== -1) {
        isFavorite = true;
        this.addContactToList(contact, 'favorite');
      }
      contactHtml = this._getHtmlContact(contact, isFavorite);
      if (contact.subscription === "subscribe") {
        $("#online-contacts-list").prepend(contactHtml);
      }
      else {
        $("#online-contacts-list").append(contactHtml);
      }
      
      $("#contacts .no-content").hide();

      if (contact.subscription === 'both') {
        user.getVCard(checkServerAddress(contact.jid));
        user.getPresence(checkServerAddress(contact.jid));
      }
    }
  },

  countContacts : function(available) {
    return $("div[contact-available='"+ available +"']").length;
  },

  addContactToList : function(contact, list) {
    var index = '',
        isFavorite = false,
        inList = $("#"+ list +"-list div[data-jid='"+ contact.jid +"']");

    if (contact.groups !== undefined) {
      index = contact.groups.indexOf(list);
    }

    if (inList.length === 0) {
      if (list === 'favorite') {
        isFavorite = true;
      }

      var contactHtml = this._getHtmlContact(contact, isFavorite);

      $('#' + list + '-list').append(contactHtml);
      $('#' + list + '-list').closest('section').children('.no-content').hide();
    }
  },

  toggleFavorite : function(jid) {
    var contact = this.getContact(jid);
    var index = contact.groups.indexOf('favorite');
    if (index !== -1) {
      contact.groups.splice(index, 1);
      this._refreshContactVariable(jid, 'favorite', false);
      $(".favorite-list div[data-jid='"+ jid +"']").remove();
    } else {
      contact.groups.push('favorite');
      this._refreshContactVariable(jid, 'favorite', true);
      this.addContactToList(contact, 'favorite');
    }

    this._addOrUpdateContact(contact);
  },

  _addOrUpdateContact: function(contact) {
    var request = $iq({type:'set', id: 'add_msg_1', from: user.jid});
    request.c('query', {xmlns:'jabber:iq:roster'});
    request.c('item',{jid: contact.jid});
    for(var i = 0; i < contact.groups.length; i++) {
      request.c('group').t(contact.groups[i] );
    }

    Connection.sendRequest(request);
  },

  _getHtmlContact : function (contact, isFavorite) {
    var isGroup = this.isGroup(contact.jid);
    var nick;
    var contactHtml = "<div data-jid='"+ contact.jid +"' class='select-contact'>";
    if (contact.photo !== null && contact.photo !== undefined){
      contactHtml    += "<figure><img src='"+contact.photo+"' class='contact-photo touch-contact' />";
    } else{
      if (!isGroup) {
        contactHtml    += "<figure class='no-photo'><img src='img/photo.png' class='contact-photo touch-contact' />";
      } else {
        contactHtml    += "<figure class='no-photo'><img src='img/group.png' class='contact-photo touch-contact' />";
      }
    }
    contactHtml    += "<span class='contact-messages'/></span></figure>";
    contactHtml    += "<div class='status'>";
    if (contact.nickname !== null && contact.nickname !== undefined) {
      contactHtml  += "<p class='contact-nickname touch-contact'>"+ contact.nickname.replace(/#/g,' ') +"<span class='new-group badge'></span></p>";
    } else {
      contactHtml  += "<p class='contact-nickname touch-contact'>"+ contact.jid.replace('@' + serverAddress, '') +"<span class='new-group badge'></span></p>";
    }
    contactHtml    += "<p class='contact-status touch-contact'>";
    contactHtml    += "<span class='last-message'></span>";
    if (contact.status === undefined) {
      contactHtml    += "<span class='status-text'></span>";
    } else {
      contactHtml    += "<span class='status-text'>"+ contact.status +"</span>";
    }
    contactHtml    += "<span class='contact-composing'>\"Esta escribiendo\"</span>";
    contactHtml    += "</p>";      
    contactHtml    += "<p class='contact-last touch-contact'></p></div>";
    if (contact.subscription === "subscribe") {
      contactHtml    += "<div class='subscription'>";
      contactHtml    += "<a href='#contact-list' class='add-contact-button'><img src='img/arrow-mdpi.png' class='add-contact-button' /></a>";
      contactHtml    += "<a href='#contact-list' class='reject-contact-button'><img src='img/cancel-mdpi.png' class='reject-subscription-button' /></a>";
      contactHtml    += "</div>";
    } else {
      if (isFavorite) {
        contactHtml  += "<div class='contact-favorite active'/>";
      } else {
        contactHtml  += "<div class='contact-favorite'/>";
      }
      contactHtml  += "<div class='contact-delete'/>";
      if (!isGroup) {
        if (contact.presence === undefined) {
          contactHtml    += "<div class='contact-presence unavailable'/>";
        } else {
          contactHtml    += "<div class='contact-presence "+ contact.presence +"'/>";
        }
      }
    }
    contactHtml    += "</div>";
    return contactHtml;
  },

  _refreshContactVariable : function (jid, variable, value) {
    $("div[data-jid='"+ jid +"']").each(function() {
      var element = $(this).find('.contact-' + variable);
      if (element.length !== 0) {
        if (variable === 'photo') {
          element[0].src = value;
        } else if (variable === 'presence') {
          element.attr('class', 'contact-presence ' + value);
        } else if (variable === 'favorite') {
          element.toggleClass('active');
        } else if (variable === 'messages' ) {
          contact = contacts.getContact(jid);
          if (contact.messages === 0) {
            element.hide();
          } else if (contact.messages <= 9){
            element.html(contact.messages).show();
          } else {
            element.html('+9').show();
          }
        } else if (variable === 'status') {
          element.find('.status-text').text(value);
          emojify.run(element.find('.status-text')[0]);
        } else {
          element[0].textContent = value;
          emojify.run(element[0]);
        }
        
      }
    });
  },

  addContact : function (target) {
    if(target === null) {
      target = $("#addContactUsername").val();
      target += "@" + serverAddress;
      $("#addContactUsername").val('');
    }
    Connection.sendRequest($pres({from:user.jid, to:target, type:'subscribe'}));
  },

  acceptSubscription : function (target) {
    Connection.sendRequest($pres({from:user.jid, to:target, type:'subscribed'}));
    this.addContact(target);
    var contact = $("div[data-jid='"+ target +"']");
    contact.find('.subscription').remove();
    html  = "<div class='contact-favorite'/>";
    html  += "<div class='contact-presence unavailable'/>";
    contact.append(html);
    user.getVCard(checkServerAddress(target));
    user.getPresence(checkServerAddress(target));
  },

  rejectSubscription : function (target) {
    var requestRoster = $iq({from:user.jid, type:'set'}).c('query', {xmlns:'jabber:iq:roster'});
    requestRoster.up().c('item', {jid:target, subscription:'remove'});
    Connection.sendRequest($pres({from:user.jid, to:target, type:'unsubscribed'}));
    Connection.sendRequest(requestRoster);
    this.removeContact(target);
  },

  removeContactFromHtml : function (target) {
    $("div[data-jid='"+ target +"']").each(function() {
      $(this).remove();
    });
  }
};

$(function() {
  $("#add-contact-button").on(clickEvent, function(e){
    e.preventDefault();
    $("body > section").hide();
    $("#contact-list").show();
    contacts.addContact(null);
  });

  $('.contact-list').on(clickEvent, '.add-contact-button', function(e) {
    e.preventDefault();
    var contactJid = $(this).closest('.select-contact').data('jid');
    contacts.acceptSubscription(contactJid);
  });

  $('.contact-list').on(clickEvent, '.reject-contact-button', function(e) {
    e.preventDefault();
    var contactJid = $(this).closest('.select-contact').data('jid');
    contacts.rejectSubscription(contactJid);
  });
});
