var connection = null;
var message = null;
var clickEvent = ('ontouchstart' in window ? 'touchend' : 'click');
var storageObject = null;
var serverAddress = "chatea.divux.com";
var conferenceAddress = 'conference.' + serverAddress;
var historyLimit = 10;
var contactLimit = 3;
var activity = ('MozActivity' in window ? MozActivity : false);
var closedSession = false;
var notificationsWithSound = 'true';

function hexToBase64(str) {
  return btoa(String.fromCharCode.apply(null, str.replace(/\r|\n/g, "").replace(/([\da-fA-F]{2}) ?/g, "0x$1 ").replace(/ +$/, "").split(" ")));
}

function getTextNodes(node){
  return $(node).contents().filter(function(){
    return (
      ((this.nodeName=="#text" && this.nodeType=="3") || this.nodeType=="4") && ($.trim(this.nodeValue.replace("\n","")) !== "")
    );
  });
}

function getLongTextNode(node) {
    var value = "";
    for (var x = 0;x < node.childNodes.length; x++) {
        value = value + node.childNodes[x].nodeValue;
    }
    return value;
}

function getNodeValue(node){
  var $textNodes = getTextNodes(node);
  textValue = ($textNodes[0]) ? $.trim($textNodes[0].textContent) : "";
  return textValue;
}

function checkServerAddress(jid){
  var array = jid.split("@");
  if(array.length==1){
    return jid + "@" + serverAddress;
  }
  else{
    return jid;
  }
}

function getPhotoValue (photo, variable) {
  var split = photo.split(";");
  if (split.length>0) {
    if (variable == "type") {
      return split[0].split(":")[1];
    }
    else if (variable == "base64") {
      return split[1].split("base64,")[1];
    }
  }
  return null; 
}

function b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 1024;

    function charCodeFromCharacter(c) {
        return c.charCodeAt(0);
    }

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);
        var byteNumbers = Array.prototype.map.call(slice, charCodeFromCharacter);
        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, {type: contentType});
    return blob;
}

function noContentMessage(element){
  var section_id = $(element).attr("id");
  if($("#"+section_id+" article").first().children('div').length) {
    $("#"+section_id+" .no-content").hide();
  } else {
    $("#"+section_id+" .no-content").show();
  }
}

function s4() {
  return Math.floor((1 + Math.random()) * 0x10000)
             .toString(16)
             .substring(1);
}

function guid() {
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
         s4() + '-' + s4() + s4() + s4();
}

function submitPhoto() {
  user.setVCard();
}

function manageAnchor(sections , anchor , section) {
  var wrapper = $('.cn-wrapper');
  var button = $('.circular-button-base');

  if (anchor.attr('href') !== ('#' + section.attr('id'))) {
    sections.each(function() {
      $(this).hide();
      noContentMessage($(this));
    });
    $(anchor.attr('href')).show();
    document.activeElement.blur();

    if ($('#settings').hasClass('expand')) {
      $('.wrap').removeClass('show-settings');
      $('#settings').removeClass('expand');
    }
  } else if (!anchor.hasClass('active')) {
    $('#' + section.attr('id') + ' > .wrap').toggleClass('show-settings');
    $('#settings').toggleClass('expand');
  }
  $('#settings').show();
  if (wrapper.hasClass("opened-nav")) {
    wrapper.removeClass("opened-nav");
    button.removeClass("button-minus");
    button.addClass("button-plus");
  }
}

function groupInvite (sender, group, receiver) {
  var invite = {timestamp: new Date(), group: group, invited: receiver};
  storeGroupInvites(invite);
  var request = $msg({from:sender, to:group}).c('x', {xmlns:'http://jabber.org/protocol/muc#user'}).c('invite',{to:receiver}).c('reason','','Invitation');
  Connection.sendRequest(request);
}

$(document).ready(function() {
  $('input[autofocus]').trigger('focus');

  var sections = $('body').children('section');
  var footer = $('body').children('footer');

  /* Show no content message */
  sections.each(function() {
    noContentMessage($(this));
  });

  $("#logout").on(clickEvent, function(event) {
    event.preventDefault();
    disconnect();
  });

  footer.on(clickEvent, 'a', function(event) {
    event.preventDefault();
    var anchor = $(this);
    var section = $(this).closest('section');
    manageAnchor(sections, anchor, section);
  });

  /* Shows tabs info */
  sections.on(clickEvent, 'a', function(event) {
    event.preventDefault();
    var anchor = $(this);
    var section = $(this).closest('section');
    manageAnchor(sections, anchor, section);
  });

  $('head').append("<link href='css/emoji.css' rel='stylesheet' rel='stylesheet' />");

  $('#profile-photo-file').on('change', user.changePhoto);

  $('.profile-photo').on(clickEvent, function() {
    if (activity) {
      var pick = activity({
        name: "pick",
        data: {
          type: ["image/png", "image/jpeg", "image/gif"]
        }
      });

      pick.onsuccess = user.handleFile;

      pick.onerror = function() {
        alert('La imagen no pudo ser procesada');
      };
    } else {
      $('#profile-photo-file').click();
    }
  });

  /* Edit User Profile */
  $('#edit-user').on(clickEvent, function() {
    var type = $(this).data('type');
    var name = $('#profile-info h1').text();
    var status = $('#profile-info p').text();
    if (status === '') {
      status = "Mensaje Personal";
    }
    if(type === 'edit') {
      var p = $('#profile-info p');
      $('#profile-info h1').html('<input type="text" id="profile_nickname" placeholder="Nombre de Usuario" value="'+ name +'" autofocus required />');
      p.first().html('<input type="text" id="profile_status" placeholder="Mensaje Personal" value="'+ status +'" />');
      p.first().next().remove();
      showPresenceOptions(p);
      $(this).find('img').attr('src','img/check.png');
      $(this).data('type', 'save');
    } else {
      var new_name = $('#profile_nickname').val();
      if (new_name === '') {
        new_name = user.jid;
      }
      var new_status = $('#profile_status').val();
      $('#profile-info h1').html(new_name);
      $('#profile-info p').first().html(new_status);
      $(this).find('img').attr('src','img/edit-white.png');
      $(this).data('type', 'edit');
      changePresence();
      user.changeNickname(new_name);
      user.changeStatus(new_status);
    }
  });

  $('#profile').on(clickEvent, function() {
    $(".circular-nav").show();
  });

  $('#profile-info').on(clickEvent, '.presence', function (e) {
    var _this = $(this).removeClass("transparent");
    _this.siblings("span").removeClass("transparent");
    refreshOptions(_this.text());
  });

  // Notifications configuration
  $('#config-notifications').on('change', function() {
    status = $(this).is(':checked')+'';
    storeSettings({jid:user.jid, setting:"notification", value:status});
  });
});