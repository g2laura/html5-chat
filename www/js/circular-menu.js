function toggleCircularButton(button) {
  var wrapper = button.siblings('.cn-wrapper');
  if (button.hasClass("button-plus")){
    button.removeClass("button-plus");
    button.addClass("button-minus");
    wrapper.addClass("opened-nav");
  } else {
    button.removeClass("button-minus");
    button.addClass("button-plus");
    wrapper.removeClass("opened-nav");
  }
}

$(function() {
  $('.circular-button-base').on(clickEvent, function (e) {
    var button = $(this);
    toggleCircularButton(button);
  });
});