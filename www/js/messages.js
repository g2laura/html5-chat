var messages = {
  LOG_TAG : "MESSAGES",

  /* Contacts list */
  listRoster : function(message){
    var roster = message.getElementsByTagName("item"),
        contact,
        jid,
        subscription;
    for(var i=0; i < roster.length; i++){
      subscription = roster[i].getAttribute("subscription");
      if (subscription === "both" || subscription === "to" || subscription === "from" || subscription === "none" || subscription === 'remove'){
        jid = roster[i].getAttribute("jid");
        if (subscription === "to" ){
          contact = {"jid" : jid, "subscription" : "subscribe" , "groups" : this._getContactGroups(roster[i].getElementsByTagName('group'))};
        }
        else {
          contact = {"jid" : jid, "subscription" : subscription, "groups" : this._getContactGroups(roster[i].getElementsByTagName('group'))};
        }
        contacts.refreshContact(contact);
      }
    }
    getUserGroups();
    getLastContactsTalk(user.jid);
  },

  /* Contacts information */
  listPresence : function(message){
    var contact = null;
    var jid = null;
    if (contacts.list.length > 0) {
      var states = message.getElementsByTagName("show"),
          type = message.getAttribute("type"),
          x = message.getElementsByTagName("x"),
          elements = message.getElementsByTagName("status");
      jid = message.getAttribute("from").split("/");
      if (contacts.isGroup(jid[0])){
        deleteGroupInvites(jid[0], jid[1]);
      } else {
        getUserInvites(jid[0]);
        contact = contacts.getContact(jid[0]);
        if (type === "unavailable") {
          contacts.setContactVariable(jid[0], "presence", "unavailable");
          if ($.inArray(jid[0],lastContacts) !== -1) {
            $("div[data-jid='"+ jid[0] +"']").find(".contact-composing").removeClass("composing");
            $('.bubble.left').last().before().find(".writing").removeClass("show-writing");
          }
        }
        else if (type === null &&  states.length === 0) {
          contacts.setContactVariable(jid[0], "presence", "available");
        }
        else if (states.length > 0) {
          var presence = getNodeValue(states[0]);
          contacts.setContactVariable(jid[0], "presence", presence);
        }
        else if (type === "subscribe") {
          contacts.setContactVariable(jid[0], "presence", "unavailable");
        }
        if (elements.length > 0) {
          var status = getNodeValue(elements[0]);
          contacts.setContactVariable(jid[0], "status", status);
        }

        if (x.length > 0) {
          var xmlns = x[0].getAttribute("xmlns");
          var photo = x[0].getElementsByTagName('photo');
          var hash;
          if (xmlns === 'vcard-temp:x:update' || photo !== undefined) {
            hash = getNodeValue(photo[0]);
            if (jid[0] === user.jid) {
              contact = user;
            }
            if ((hash !== "" && contact.hash !== undefined && hash != contact.hash) ||
                contact.hash === undefined) {
              contact.hash = hash;
              user.getVCard(checkServerAddress(jid[0]));
            }
          }
        }
      }
    }
    if (message.getAttribute("type") === "subscribe") {
      jid = message.getAttribute("from").split("/")[0];
      contact = contacts.getContact(jid);
      if (contact === null) {
        contact = {"jid" : jid, "subscription" : "subscribe", "groups" : null};
      }
      else {
        contact = {"jid" : jid, "subscription" : "subscribing", "groups" : null};
      }
      contacts.refreshContact(contact);
    }
  },

  /* Nickname, status and contact avatar */
  listVCard : function(message) {
    var jid = message.getAttribute("from"),
        elements = message.getElementsByTagName("vCard"),
        contact = null,
        nickname = null,
        image = null;

    if (elements.length > 0) {
      var childNodes = elements[0].childNodes;
      if (childNodes.length > 0) {
        for (var i = 0; i < childNodes.length; i++) {
          var node = childNodes[i];
          if (node.nodeName === "NICKNAME") {
            nickname = getNodeValue(node);
          } else if (node.nodeName === "PHOTO") {
            if(node.childNodes.length > 0) {
              var imageType = null;
              var base64 = null;
              for (var k = 0; k < node.childNodes.length; k++) {
                if (node.childNodes[k].nodeName === "BINVAL") {
                  base64 = getLongTextNode(node.childNodes[k]);
                }
                else if (node.childNodes[k].nodeName === "TYPE"){
                  imageType = getNodeValue(node.childNodes[k]);
                }
              }
              image = "data:"+imageType+";base64," + base64;
            }
          }
        }
      }
    }

    if(user.jid !== jid) {
      if (nickname !== null && nickname !== "") {
        contacts.setContactVariable(jid, 'nickname', nickname);
      }
      if (image !== null) {
        contacts.setContactVariable(jid, 'hash', hex_sha1(image));
        contacts.setContactVariable(jid, 'photo', image);
      }
    } else {
      if (nickname !== null) {
        user.nickname = nickname;
      }
      if (image !== null) {
        user.photo = image;
      }
      user.refreshProfile();
    }
  },

  _getContactGroups : function(xmlGroups) {
    var groups = [];
    for (var j = 0; j < xmlGroups.length; j++){
      groups.push(xmlGroups[j].textContent);
    }
    return groups;
  }
};

function determineMessage(message) {
  var queryArray = message.getElementsByTagName("query");

  if (queryArray.length > 0) { // Contact list
    if (queryArray[0].getAttribute("xmlns") === "jabber:iq:roster") {
      messages.listRoster(message);
    }
    else{
      storageObject = message;
      user.getStorageVariable();
    }
  }
  if (message && message.tagName === "iq") {
     if (message.getAttribute("id") === "signup") {
      if(message.getElementsByTagName("error").length > 0 ) {
        $(".alert-success").html("");
        $(".alert-success").hide();
        $("#signup .alert-error").html("Usuario ingresado ya existe en el sistema.");
        $("#signup .alert-error").show();
      }
      else
      {
        $(".alert-error").html("");
        $(".alert-error").hide();
        $("#login .alert-success").html("Usuario registrado exitosamente.");
        $("#login .alert-success").show();
        $("#signup").hide();
        $("#login").show();
      }
    }
  }
}