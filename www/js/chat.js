var chat = {
  target : {
    jid : null,
    name : null,
    status : null,
    historyCount : null,
    writing : null,
    inputCount : null,
  },

  // Shows chat message on chat box
  showMessage : function (jid, content, time, type, messageType, messagePosition, id, status, chatType, name) {
    jid = jid.split("/");
    var contact, messageNick;
    name = name.split("@")[0];
    if (chatType === "groupchat") {
      contact = contacts.getContact(name+"@"+serverAddress);
    } else {
      contact = contacts.getContact(jid[0]);
    }
    if(contact !== null) {
      if (contact.nickname !== null && contact.nickname !== undefined) {
        messageNick = contact.nickname;
      } else {
        messageNick = contact.jid.replace('@' + serverAddress,"");
      }
    } else {
      messageNick = name;
    }
    time = this.getTime(time);
    if ((jid[0] === $('#chat .title').attr('data-jid') && $('#chat').css('display') !== 'none') ||
        jid[0] === user.jid) {

      if(type === "right") {
        message = this.rightBubleHtml(id, status, content, time, chatType);
      } else {
        if (contact !== null && contact.photo !== null && contact.photo !== undefined){
          message = this.leftBubleHtml(contact.photo,content,time,"", chatType, messageNick);
        } else {
          message = this.leftBubleHtml('img/photo.png',content,time,"no-photo", chatType, messageNick);
        }
      }

      if (messagePosition == "bottom") {
        $("#messages").append(message);
        emojify.run($('.message').last()[0]);
      } else {
        $("#messages").prepend(message);
        emojify.run($('.message').first()[0]);
      }
      
      if(messageType == "send") {
        $("#msg").val("");
      }

      $("#messages")[0].scrollTop = $("#messages")[0].scrollHeight;
    } else {
      if (jid[0] !== null && jid[0] !== undefined) {
        if(contact.messages === null || contact.messages === undefined || isNaN(contact.messages)){
          contacts.setContactVariable(jid[0], "messages", 1);
        } else {
          contacts.setContactVariable(jid[0], "messages", contact.messages + 1);
          if (notificationsWithSound == 'true') {
            $("#new-message-sound")[0].load();
            $("#new-message-sound")[0].play();
          }
        }
      }
    }
  },
  // Sends a chat message
  sendMessage : function(sender, receiver, content, type) {
    var time = new Date(),
        id = "msg-" + guid(),
        request = '',
        chatType ='';

    if (receiver.split("@")[1] === conferenceAddress) {
      chatType = "groupchat";
      this.showMessage(sender,content,time,type,"send", "bottom", id, 'sended', chatType, "");
      request = $msg({from:sender, to:receiver, id:id, type:'groupchat'});
      request.c('body', {}, content);
    } else {
      chatType = "chat";
      this.showMessage(sender,content,time,type,"send", "bottom", id, 'sended', chatType, "");
      request = $msg({from:sender, to:receiver, id:id, type:'chat'});
      request.c('body', {}, content).c('active', {xmlns:'http://jabber.org/protocol/chatstates'});
      request.up().c('request', {xmlns:'urn:xmpp:receipts'});
    }
    Connection.sendRequest(request);
    storeMessage({timestamp: time, jid:receiver, message: content, type: "sended", user: user.jid, id: id, status: 'sended', chatType: chatType, nick: ""});
  },
  // Gets message receive form contact and show it on chat box
  getMessage : function(message) {
    var jid, response, nickname, id, fromUserToGroup, messageNick, isGroup;
    var time = new Date();
    var chatType = message.getAttribute("type");

    if (message.getElementsByTagName("body").length > 0) {
      response = message.getElementsByTagName("body")[0].childNodes[0].nodeValue;
      jid = message.getAttribute("from").split("/");
      fromUserToGroup = this.isUserMessage(jid);
      isGroup = contacts.isGroup(jid[0]);
      id = message.getAttribute('id');

      if (!isGroup) {
        this.processReceivedMessage(message, jid, response, time, chatType, id);
      } else {
        var delay = message.getElementsByTagName("delay").length;
        if (delay === 0){
          if (id !== null && !fromUserToGroup) {
            this.processReceivedMessage(message, jid, response, time, chatType, id);
          }
        } else {
          showDelayMessage(message, jid, response, time, chatType, id);
        }
      }
    } else {
      var nick = message.getElementsByTagName("nick"),
          received = message.getElementsByTagName('received'),
          invite = message.getElementsByTagName('invite');
      if (nick.length > 0) {
        nickname = getNodeValue(nick[0]);
        jid = message.getAttribute("from").split("/");
        contacts.setContactVariable(jid[0], "nickname", nickname);
      } else if (received.length > 0) {
        var messageId = received[0].getAttribute('id');
        updateMessageStatus(messageId, 'received');
        $('#' + messageId).find('.sended').addClass('received');
      } else if (invite.length > 0) {
        var groupName = message.getAttribute('from');
        var groupJson = {jid: groupName, owner:'', member: user.jid, createdAt: new Date(), nickname: groupName.split("-")[0]};
        var exists = existsGroup(groupName);
        if (!exists) {
          storeGroup(groupJson);
        }
      } else {
        if (jid !== undefined){
          contacts.setContactVariable(jid[0], "nickname", jid[0]);
        }
      }
    }
    this.processNotifications(message, chatType);

    if (jid !== undefined){
      getLastMessage(user.jid, jid[0]);
    }
  },
  // Returns html template for message photo
  leftBubleHtml : function(photo, content, time, style, chatType, name) {
    var message = "<div class='bubble left'>"; 
    message += " <img src='"+photo+"' class='"+ style +"' />";
    message += " <img class='writing' src='img/writing-chat.png'/>";
    message += " <div class='message'>";
    if (chatType === "groupchat") {
      message += "<div class='username-groupchat'>"+name+"</div>";
    }
    message += content;
    message += "<div class='time'>"+ time +"</div></div>";
    return message;
  },

  rightBubleHtml : function(id, status, content, time, chatType) {
    var message = "<div class='bubble right' id="+ id +">";
    if ( chatType === "chat") {
      message += "<div class='message-status " + status + "'/>";
    }
    message += "<div class='message from'>"+ content;
    message += "<div class='time'>"+ time +"</div></div>";
    message += "<img src='img/terra/bubble-right.png'></img></div>";
    return message;
  },

  // Cleans message section form chat
  clean : function() {
    $("#messages").empty();
  },
  // Refresh Nickname Title on chat screen
  refreshChatNickname : function(jid) {
    var contact = contacts.getContact(jid),
        nickname = $('#chat .contact-nickname');

    nickname.parent().attr('data-jid', contact.jid);

    if (contact.nickname !== null && contact.nickname !== undefined) {
      nickname[0].textContent = contact.nickname.replace(/#/g,' ');
    } else {
      nickname[0].textContent = contact.jid;
    }
    emojify.run(nickname[0]);
  },
  // Determines if a date is earlier than the current  
  compareDate: function (time) {
    var before = false;
    var currentDate = new Date();
    if (currentDate.getFullYear() !=  time.getFullYear() || currentDate.getMonth() !=  time.getMonth() ||  currentDate.getDay() !=  time.getDay() ){
      before = true;
    }
    return before;
  },

  getTime: function(time) {
    if (typeof time === "string") {
      time = new Date(time);
    }
    var before = this.compareDate(time);
    if (before) {
      time = time.format("dd/mm/yy");
    } else {
      time = time.format("h:MM tt");
    }
    return time;
  },
  // Send composing notification to contact
  sendNotification: function(sender, receiver, type) {
    var isGroup = contacts.isGroup(receiver);
    if (!isGroup){
      var request = $msg({from:sender, to:receiver, type:'chat'}).c(type, {xmlns:'http://jabber.org/protocol/chatstates'});
      Connection.sendRequest(request);
    }
  },

  messageACK : function(jid, id) {
    var isGroup = contacts.isGroup(jid);
    if (!isGroup) {
      var request = $msg({from:user.jid+'/'+user.resource, to:jid, id:'msg-'+guid()});
      request.c('received',{xmlns:'urn:xmpp:receipts', id:id});
      Connection.sendRequest(request);
    }
  },

  _updateComposingNotification : function(message, type) {
    var jid = message.getAttribute("from").split("/");
    var contact = $(".talk-list div[data-jid='"+ jid[0] +"']");
    var composing = contact.find(".contact-composing");
    var lastMessage = contact.find('.last-message');
    var lastContactMessage = $('.bubble.left');
    if ($.inArray(jid[0],lastContacts) !== -1) {
      if (type === "add") {
        lastContactMessage.last().find(".writing").addClass("show-writing");
        lastMessage.hide();
        composing.addClass('composing');
      } else if (type === "remove") {
        lastContactMessage.find(".writing").removeClass("show-writing");
        lastMessage.show();
        composing.removeClass('composing');
      }
    }
  },

  updateMessagesStatus : function(message) {
    var jid = message.getAttribute("from").split("/");
    var contact = $(".chat div[data-jid='"+ jid[0] +"']");
    if (contact.length > 0) {
      var messages = contact.closest('section').find('.received');
      messages.each(function () {
        $(this).addClass('seen');
      });
    }
    updateMessageStatusBulk(jid[0], 'seen');
  },

  createChatGroup : function(name, invites) {
    var roomName = name + '-' + guid();
    var request = $iq({to: roomName, from: user.jid + '/' + user.resource, type: 'set'}).c('query', {xmlns: 'http://jabber.org/protocol/muc#owner'}).c('x', {xmlns: 'jabber:x:data', type: 'submit'});
    var group = {jid: roomName+"@"+conferenceAddress, owner: user.jid, member: user.jid, createdAt: new Date(), nickname: roomName.split("-")[0]};
    Connection.sendRequest(request);
    this.sendPresenceToGroup(roomName + '@' + conferenceAddress );
    storeGroup(group);
    $.each(invites, function(index, guest){
      groupInvite (user.jid, roomName+"@"+conferenceAddress, guest+"@"+serverAddress);
    });
    groupParticipants = [];
    $("#participants").empty();
    $("#contactsList").empty();
  },

  sendPresenceToGroup: function(groupjid) {
    request = $pres({from: user.jid + '/' + user.resource, to: groupjid + '/' + user.jid});
    Connection.sendRequest(request);
  },

  isUserMessage: function(jid) {
    var jidToValidate = jid[0].split("@")[1];
    var res = false;
    if (jidToValidate === conferenceAddress) {
        if (jid[1] === user.jid) {
          res = true;
        }
    } 
    return res;
  },

  processReceivedMessage: function(message, jid, response, time, chatType, id) {
    this.showMessage(jid[0], response, time,"left", "receive","bottom", "", "", chatType, jid[1]);
    storeMessage({timestamp: time, jid: jid[0], message: response , type: "received", user: user.jid, id: id, status: "", chatType: chatType, nick: jid[1] });
    this.messageACK(jid[0], id);
  },

  processNotifications: function(message, chatType) {
    if (chatType !== "groupchat") {
      if (message.getElementsByTagName("composing").length > 0) {
        this._updateComposingNotification(message, "add");
      } else if (message.getElementsByTagName('paused').length > 0 ||
        message.getElementsByTagName('gone').length > 0) {
        this._updateComposingNotification(message, "remove");
      } else if (message.getElementsByTagName("active").length > 0) {
        this.updateMessagesStatus(message);
      }
    }
  },

  findContactsByValue: function(value) {
    var contactsFound = [];
    var jid, isGroup;
    $.each(contacts.list, function(index, contact){
      jid = contact.jid.split("@")[0];
      isGroup = contacts.isGroup(contact.jid);
      if (!isGroup && jid.search(value) !== -1){
        contactsFound.push(contact.jid);
      }
    });
    contactsFound.sort();
    if (contactsFound.length > 5) {
      contactsFound.splice(5);
    }
    return contactsFound;
  },

  getListContactHtml: function(jid, index, length) {
    var contact, html;
    var round ='';
    if (index === length-1) {
      round = "round-corners ";
    }
    contact = contacts.getContact(jid);
    html =  "<div class='cl-contact "+round+ "'>";
    html +=   "<span class='cl-name'>"+contact.jid.split("@")[0]+"</span>";
    html +=   "<span class='cl-icon'><img src='img/add-chat.png' /></span>";
    html += "</div>";
    return html;
  }
};

$(function() {
  $('.contact-list, .favorite').on(clickEvent, '.contact-favorite', function (e) {
    e.stopImmediatePropagation();
    var _this = $(this).parent();
    if ($(this).hasClass('active') && confirm('¿Quitar de favoritos?')) {
      contacts.toggleFavorite(_this.data("jid"));
    } else if (!$(this).hasClass('active')) {
      contacts.toggleFavorite(_this.data("jid"));
    }
  });

  $('.last-chats').on(clickEvent, '.contact-delete', function (e) {
    e.preventDefault();
    $(this).addClass("active");
    var groupJid = $(this).parent().data("jid");
    var isGroup = groupJid.indexOf(conferenceAddress);
    if (isGroup !== -1 && confirm('¿Eliminar conversación?')) {
      contacts.removeGroup(groupJid);
      deleteGroup(groupJid);
      deleteGroupMessages(user.jid, groupJid);
      $(this).parent().remove();
    }
    $(this).removeClass("active");
  });

  $('.contact-list, .favorite, .last-chats').on(clickEvent, '.touch-contact', function (e) {
    e.preventDefault();
    $(".circular-nav").hide();
    var _this = $(this).parent().parent();
    var grandpha = $(this).parent().parent().parent();
    chat.target.jid = _this.data("jid");
    chat.target.name = _this.data("name");
    chat.target.status = _this.data("status");
    chat.target.historyCount = 1;
    chat.target.writing = false;    
    chat.clean();
    if (grandpha.hasClass('favorite')) {
      $('#chat a').attr({href: '#favorite'});
    } else if (grandpha.hasClass('contact-list')) {
      $('#chat a').attr({href: '#contact-list'});
    } else if (grandpha.hasClass('last-chats')) {
      $('#chat a').attr({href: '#last-chats'});
    }
    showLastTalk(chat.target.jid ,0, historyLimit);
    $("#chat").show();
    $('#history').show();
    chat.refreshChatNickname(chat.target.jid);
    $("#contact-list, .favorite, #last-chats").hide();
    contacts.setContactVariable(chat.target.jid, "messages", 0);
    var isContactWriting = $("#talk-list div[data-jid='"+ chat.target.jid +"']").find(".contact-composing").hasClass("composing");
    if (isContactWriting) {
      $('.bubble.left').last().find(".writing").addClass("show-writing");
    } else {
      $('.bubble.left').last().prev().find(".writing").removeClass("show-writing");
    }
    if (contacts.isGroup(chat.target.jid)) {
      chat.sendPresenceToGroup(chat.target.jid);
       _this.find(".new-group").remove();
    }
  });

  $('#chat a').on(clickEvent, function() {
    chat.sendNotification(user.jid, chat.target.jid, "gone");
  });

  $('#msg').on("keydown", function(event) {
    if(event.keyCode == '13') {
      event.preventDefault();
      $('#msg-button').click();
      chat.target.writing = false;
    } else {
      if (chat.target.writing === false) {
        chat.target.writing = true;
        chat.sendNotification(user.jid, chat.target.jid, "composing");
      }
    }
  });

  $('#msg').on("keyup", function(event) {
    if ($(this).val().length === 0 && chat.target.writing === true) {
      chat.target.writing = true;
      chat.sendNotification(user.jid, chat.target.jid, "paused");
    }
    chat.target.inputCount = $(this).val().length;
    setTimeout(function(){
      if ($('#msg').val().length === chat.target.inputCount && chat.target.writing === true) {
        chat.target.writing = false;
        chat.sendNotification(user.jid, chat.target.jid, "paused");
      }
    }, 2500);
  });

  $('#chat-message').submit(function(e) {
    e.preventDefault();
    var msg = $("#msg").val().trim();
    if (msg !== "") {
      chat.sendMessage(user.jid+"/"+user.resource, chat.target.jid, msg, "right");
      chat.sendNotification(user.jid, chat.target.jid, "paused");
    }
  });

  openDb();
  $(".circular-nav").hide();

  $('#history').on(clickEvent, function () {
    if ( (chat.target.historyCount * historyLimit) >= lastMessages.length ) {
      $('#history').hide(); 
      $('#messages').addClass("adjust");
      $("#messages").prepend("<div class='more-empty'>No hay mas mensajes que mostrar</div>");
    } else {
      var start = chat.target.historyCount * historyLimit;
      var end =  (chat.target.historyCount * historyLimit) + historyLimit; 
      chat.target.historyCount++;
      showLastTalk(chat.target.jid ,start,end);
    }
  });

  $('.chat').on(clickEvent, 'a', function (e) {
    e.preventDefault();
    $(".circular-nav").show();
  });

  $('#create-chat-group').on(clickEvent, function(e) {
    e.preventDefault();
    var groupName = $('#groupName').val();
    groupName = groupName.replace(/ /g,'#');
    var groupInvite = $('#groupInvite').val().split(",");
    if ($.trim(groupName).length > 0) {
      chat.createChatGroup(groupName, groupParticipants);
    }
    $('#groupName').val("");
    $('#groupInvite').val("");
  });

  $('#groupInvite').on("keyup", function(e) {
    var name = $('#groupInvite').val();
    var list, contact, html;
    $("#contactsList").empty();
    if (name.length > 0) {
      list = chat.findContactsByValue(name);
      $.each(list, function(index, jid){
        html = chat.getListContactHtml(jid, index, list.length);
        $("#contactsList").append(html);
      });
    }

  });

 $('.create-group').on(clickEvent, '.cl-icon', function (e) {
    var jid = $(this).prev().text();
    var contact = contacts.getContact(jid+"@"+serverAddress);
    var html;
    if (contact.photo !== null && contact.photo !== undefined) {
      html =   "<span class='cl-photo' data-cljid='"+jid+"'><figure><img src='"+contact.photo+"' data-toggle='tooltip' title='"+contact.jid.split("@")[0]+"'/></figure><img class='cl-delete' src='img/delete2-hdpi.png'/></span>";
    } else {
      html =   "<span class='cl-photo' data-cljid='"+jid+"'><figure><img src='img/photo.png' /></figure><img class='cl-delete' src='img/delete2-hdpi.png'/></span>";
    }
    if ($.inArray(jid, groupParticipants) === -1 ){
      $("#participants").append(html);
      groupParticipants.push(jid);
    }
    $("#contactsList").empty();
  });

  $('.create-group').on(clickEvent, '.cl-photo', function (e) {
    var jid = $(this).data('cljid');
    $(this).remove();
    groupParticipants.splice($.inArray(jid, groupParticipants),1);
  });

});